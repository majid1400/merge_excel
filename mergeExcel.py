import glob
import pandas as pd


def concatfile(name):
	fileConcat = pd.concat([pd.read_csv(f) for f in glob.glob("path/*/%s_*.csv" % name)], ignore_index=True)
	fileConcat.drop(['0'], axis=1, inplace=True)
	a = pd.DataFrame(fileConcat)
	a.to_csv('%s.csv' % name, index=False)


if __name__ == "__main__":
	concatfile('follower')
	concatfile('following')
